package com.andri.sample.rest;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import com.andri.sample.version.ApiVersion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getPerson(){

		String result = "this is rest person api version 1";
		return getJsonResponse(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/person", method = RequestMethod.GET)
	@ResponseBody
	@ApiVersion(2)
	public ResponseEntity<String> getPersonV2(){

		String result = "this is rest person api version 2";

		return getJsonResponse(result, HttpStatus.OK);
	}


	public static <T> ResponseEntity<T> getJsonResponse(T src, HttpStatus status) {

		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		return new ResponseEntity<T>(src, headers, status);
	}
	
}
